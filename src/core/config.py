from pydantic import BaseSettings, RedisDsn


class Settings(BaseSettings):
    REDIS_DSN: str = "redis://0.0.0.0:6379/"


settings = Settings()
