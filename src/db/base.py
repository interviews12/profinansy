from abc import ABC, abstractmethod


class BaseDatabaseClient(ABC):
    def __init__(self, url: str, *args, **kwargs):
        self.url = url

    @abstractmethod
    async def connect(self):
        raise NotImplementedError

    @abstractmethod
    async def set(self, name: str, data: dict):
        raise NotImplementedError

    @abstractmethod
    async def get(self, name: str):
        raise NotImplementedError

    @abstractmethod
    async def disconnect(self):
        raise NotImplementedError
