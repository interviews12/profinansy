import aioredis
from loguru import logger

from core.config import settings
from db.base import BaseDatabaseClient


class RedisDB(BaseDatabaseClient):
    def __init__(self, url: str, *args, **kwargs):
        super(RedisDB, self).__init__(url, *args, **kwargs)
        self.connection = None

    async def connect(self):
        if not self.connection:
            self.connection = aioredis.from_url(
                self.url, encoding="utf-8", decode_responses=True
            )

    async def set(self, name: str, data: dict):
        try:
            await self.connection.set(name, data)
        except Exception as e:
            logger.error(e)

    async def get(self, name: str):
        try:
            value = await self.connection.get(name)
            return value
        except Exception as e:
            logger.error(e)

    async def disconnect(self):
        if self.connection:
            await self.connection.close()


client_db = RedisDB(url=settings.REDIS_DSN)
