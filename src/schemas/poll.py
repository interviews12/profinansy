import uuid
from typing import List

from pydantic import BaseModel

# -------------------------
# Input schemas
# -------------------------


class PollInputSchema(BaseModel):
    title: str
    options: List[str]


class PollOptionInputSchema(BaseModel):
    pk: str


# -------------------------
# Output schemas
# -------------------------


class PollOptionsOutputSchema(BaseModel):
    name: str
    count: int = 0


class PollOptionResultOutputSchema(BaseModel):
    pk: str
    name: str
    count: int = 0
    percentage: float = 0


class PollValidatedDataSchema(BaseModel):
    title: str
    options: List[PollOptionsOutputSchema]


class PollResultOutputSchema(BaseModel):
    pk: str
    title: str
    options: List[PollOptionResultOutputSchema]
