from typing import List

from redis_om import JsonModel


class PollOptions(JsonModel):
    name: str
    count: int = 0


class Poll(JsonModel):
    title: str
    options: List[PollOptions]
