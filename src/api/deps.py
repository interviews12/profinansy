from typing import List, Tuple

from fastapi import HTTPException
from loguru import logger
from redis_om import NotFoundError

from models.poll import Poll, PollOptions
from schemas.poll import (
    PollInputSchema,
    PollOptionInputSchema,
    PollResultOutputSchema,
    PollValidatedDataSchema,
)


async def get_poll_validated_data(poll: PollInputSchema) -> PollValidatedDataSchema:
    """Validate and return validated data"""

    if not poll.options:
        raise HTTPException(status_code=400, detail="Specify the answer options.")

    data = {"title": poll.title, "options": [{"name": item} for item in poll.options]}

    return PollValidatedDataSchema(**data)


async def get_vote(pk: str, vote_pk: PollOptionInputSchema) -> Tuple[Poll, PollOptions]:
    """Return the poll and selected vote"""
    try:
        poll = Poll.get(pk)
        vote = None
        for option in poll.options:
            if option.pk == vote_pk.pk:
                vote = option

        if not vote:
            raise HTTPException(status_code=404, detail="Not found")
    except (NotFoundError, HTTPException) as e:
        raise HTTPException(status_code=404, detail="Not found")
    except Exception as e:
        logger.error(e)
        # Can send to Sentry
        raise HTTPException(status_code=500, detail="An unknown error has occurred")

    return poll, vote


def get_poll_option_results(votes_count: int, options: List[dict]) -> List[dict]:
    """Return poll results with percentage value"""

    def set_percentage(option: dict, percentage: float) -> dict:
        """Set percentage value"""
        option["percentage"] = round(percentage * 100, 2)
        return option

    # The percentage of votes varies between [99.99, 100.01]
    if not votes_count:
        results = [set_percentage(option, 0) for option in options]
    else:
        results = [
            set_percentage(option, option.get("count", 0) / votes_count)
            for option in options
        ]

    return results


async def get_poll_results(pk: str) -> PollResultOutputSchema:
    """Return the poll results with percentage"""

    try:
        poll = Poll.get(pk)
        data = poll.dict()

        votes_count = sum([item.get("count", 0) for item in data.get("options", [])])
        options_data = get_poll_option_results(
            votes_count=votes_count, options=data.get("options", [])
        )
        data["options"] = options_data

        return PollResultOutputSchema(**data)

    except NotFoundError as e:
        raise HTTPException(status_code=404, detail="Not found poll")
    except Exception as e:
        logger.error(e)
        # Can send the errors to Sentry
        raise HTTPException(status_code=500, detail="An unknown error has occurred")
