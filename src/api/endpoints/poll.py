from typing import Tuple

from fastapi import APIRouter, Depends

from api.deps import get_poll_results, get_poll_validated_data, get_vote
from models.poll import Poll, PollOptions
from schemas.poll import PollResultOutputSchema, PollValidatedDataSchema

poll_router = APIRouter()


@poll_router.post("/", response_model=Poll, status_code=201)
async def create_poll(
    validated_data: PollValidatedDataSchema = Depends(get_poll_validated_data),
):
    return Poll(**validated_data.dict()).save()


@poll_router.get("/{pk}/", response_model=PollResultOutputSchema)
async def get_poll_results(result: PollResultOutputSchema = Depends(get_poll_results)):
    return result


@poll_router.post("/{pk}/vote/", response_model=Poll)
async def vote_in_poll(pk: str, result: Tuple[Poll, PollOptions] = Depends(get_vote)):
    # The answer may not be counted correctly
    # if 2 users vote at the same time
    poll, vote = result
    vote.count = vote.count + 1
    poll.save()

    return Poll.get(pk=pk)
