import logging

import uvicorn
from fastapi import FastAPI

from api.endpoints.poll import poll_router
from db.redis import client_db

app = FastAPI(
    title="Pro Finansy test task",
    docs_url="/api/swagger",
    openapi_url="/api-ws/openapi.json",
)


@app.on_event("startup")
async def startup():
    await client_db.connect()


@app.on_event("shutdown")
async def shutdown():
    await client_db.disconnect()


app.include_router(poll_router, prefix="/poll", tags=["poll"])

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host="0.0.0.0",
        port=8000,
        log_level=logging.INFO,
        ws_ping_interval=None,
        ws_ping_timeout=None,
    )
